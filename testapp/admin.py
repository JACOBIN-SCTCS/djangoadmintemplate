from typing import Any
from django.contrib import admin
from django.http import HttpRequest
from .models import Employee, Entity, Document, EmployeeDocument
from django.db import models

# import textwidget 
from django.forms.widgets import TextInput

# Register your models here.

################################################################

#### PART 1
# admin.site.register(Employee)
# admin.site.register(Entity)
# admin.site.register(Document)
# admin.site.register(EmployeeDocument)


################################################################

# Part 2 EmployeeAdmin
# class EmployeeAdmin(admin.ModelAdmin):
#     pass
#     # UNCOMMENT 2
#     #list_display = ['empcode', 'empname', 'emp_entity']

#     # UNCOMMENT 1
#     #search_fields = ['empcode', 'empname']

#     # UNCOMMENT 3
#     #ordering = ['empcode']

#     # UNCOMMENT 4
#     #ordering = ['-empcode']


# admin.site.register(Employee,EmployeeAdmin)
# admin.site.register(Entity)
# admin.site.register(Document)
# admin.site.register(EmployeeDocument)

################################################################

# # Part 3 EmployeeAdmin
# class EmployeeAdmin(admin.ModelAdmin):

#     def doc_count(self, obj):
#         return obj.employeedocument_set.count()
    
#     list_display = ['empcode', 'empname', 'emp_entity','doc_count']





# admin.site.register(Employee,EmployeeAdmin)
# admin.site.register(Entity)
# admin.site.register(Document)
# admin.site.register(EmployeeDocument)

################################################################

# # Part 4 EmployeeAdmin
# class EmployeeAdmin(admin.ModelAdmin):
#     list_display = ['empcode', 'empname', 'emp_entity']

    

#     fields = ['empcode', 'empname', 'emp_entity']

#     # UNCOMMENT 1 (comment fields first)
#     # fieldsets = [
#     #     (
#     #         "Basic Details",
#     #         {
#     #             "fields": ["empcode", "empname"],
#     #         },
#     #     ),
#     #     (
#     #         "Enitity Details",
#     #         {
#     #             #"classes": ["collapse"],
#     #             "fields": ["emp_entity"],
#     #         },
#     #     ),
#     # ]



# class DocumentAdmin(admin.ModelAdmin):

#     list_display = ['doc_name', 'doc_status']

#     def mark_published(self, request, queryset):
#         for doc in queryset:
#             doc.doc_status = not doc.doc_status
#             doc.save()
#     actions = ['mark_published']


# admin.site.register(Employee,EmployeeAdmin)
# admin.site.register(Entity)
# admin.site.register(Document,DocumentAdmin)
# admin.site.register(EmployeeDocument)

################################################################

## Part 5
# class EmployeeAdmin(admin.ModelAdmin):
#     list_display = ['empcode', 'empname', 'emp_entity']
#     fields = ['empcode', 'empname', 'emp_entity']

   
# class DocumentAdmin(admin.ModelAdmin):

#     list_display = ['doc_name', 'doc_status']
#     actions = ['mark_published']

#     formfield_overrides = {
#         models.TextField: {"widget": TextInput(attrs={"size": "100"})},
#     }
    
# admin.site.register(Employee,EmployeeAdmin)
# admin.site.register(Entity)
# admin.site.register(Document,DocumentAdmin)
# admin.site.register(EmployeeDocument)

################################################################

# ## Part 6 (Readonly fields to staff user)
# class EmployeeAdmin(admin.ModelAdmin):
#     list_display = ['empcode', 'empname', 'emp_entity']
#     fields = ['empcode', 'empname', 'emp_entity']

   
# class DocumentAdmin(admin.ModelAdmin):

#     list_display = ['doc_name', 'doc_status']
#     actions = ['mark_published']

#     def get_readonly_fields(self, request, obj):
#         if request.user.is_staff:
#             if request.user.is_superuser:
#                 return[]
#             else:
#                 return ['doc_status']
        
    
# admin.site.register(Employee,EmployeeAdmin)
# admin.site.register(Entity)
# admin.site.register(Document,DocumentAdmin)
# admin.site.register(EmployeeDocument)
############################################