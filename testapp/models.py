from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Employee(models.Model):
    empcode = models.CharField(max_length=7, primary_key=True)
    empname = models.CharField(max_length=50)
    emp_entity = models.ForeignKey('Entity', on_delete=models.CASCADE)
    emp_user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True,blank=True)

    def __str__(self):
       return "{} - {}".format(self.empcode, self.empname)

class Entity(models.Model):
    entitycode = models.CharField(max_length=3, primary_key=True)
    entityname = models.CharField(max_length=50)

    def __str__(self):
        return "{} - {}".format(self.entitycode, self.entityname)

class Document(models.Model):
    doc_id = models.AutoField(primary_key=True)
    doc_name = models.CharField(max_length=50)
    doc_description = models.TextField(max_length=100,default='',blank=True)
    doc_status = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.doc_name)

class EmployeeDocument(models.Model):
    empcode = models.ForeignKey('Employee', on_delete=models.CASCADE)
    doc_id = models.ForeignKey('Document', on_delete=models.CASCADE)
    doc_path = models.CharField(max_length=100)

    def __str__(self):
        return "{} ({})".format(self.doc_id.doc_name,self.empcode.empname)
